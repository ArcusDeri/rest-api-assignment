﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagement.Models;

namespace UserManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/UserGroups")]
    public class UserGroupsController : Controller
    {
        private readonly FaustDBContext _context;

        public UserGroupsController(FaustDBContext context)
        {
            _context = context;
        }

        // GET: api/UserGroups
        [HttpGet]
        public IEnumerable<UserGroup> GetUserGroup()
        {
            return _context.UserGroup;
        }

        // Get groups to which user belongs.
        // GET: api/UserGroups/User/{uid}
        [HttpGet("User/{uid}")]
        public string[] GetUsersGroups([FromRoute] int uid)
        {
            var groupsIds = _context.UserGroup
                .Where(x => x.UserId == uid)
                .Select(x => x.GroupId);
            var userGroups = _context.Group.Where(x => groupsIds.Contains(x.Id));

            var groupStrings = userGroups.Select(group => group.Name.Trim()).ToArray();
            return groupStrings;
        }

        // Get users belonging to a given group.
        // GET: api/UserGroups/Group/{gid}
        [HttpGet("Group/{gid}")]
        public string[] GetGroupsUsers([FromRoute] int gid)
        {
            var usersIds = _context.UserGroup
                .Where(x => x.GroupId == gid)
                .Select(x => x.UserId);
            var users = _context.User.Where(x => usersIds.Contains(x.Id));
            var groupsStrings = users.Select(user => user.UserName.Trim()).ToArray();

            return groupsStrings;
        }

        // GET: api/UserGroups/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserGroup([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userGroup = await _context.UserGroup.SingleOrDefaultAsync(m => m.Id == id);

            if (userGroup == null)
            {
                return NotFound();
            }

            return Ok(userGroup);
        }

        // PUT: api/UserGroups/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserGroup([FromRoute] int id, [FromBody] UserGroup userGroup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userGroup.Id)
            {
                return BadRequest();
            }

            _context.Entry(userGroup).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserGroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserGroups
        [HttpPost]
        public async Task<IActionResult> PostUserGroup([FromBody] UserGroup userGroup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserGroup.Add(userGroup);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserGroup", new { id = userGroup.Id }, userGroup);
        }

        // DELETE: api/UserGroups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserGroup([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userGroup = await _context.UserGroup.SingleOrDefaultAsync(m => m.Id == id);
            if (userGroup == null)
            {
                return NotFound();
            }

            _context.UserGroup.Remove(userGroup);
            await _context.SaveChangesAsync();

            return Ok(userGroup);
        }

        private bool UserGroupExists(int id)
        {
            return _context.UserGroup.Any(e => e.Id == id);
        }
    }
}