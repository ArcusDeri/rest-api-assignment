﻿using System;
using System.Collections.Generic;

namespace UserManagement.Models
{
    public partial class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
