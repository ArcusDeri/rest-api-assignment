﻿using System;
using System.Collections.Generic;

namespace UserManagement.Models
{
    public partial class UserGroup
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}
