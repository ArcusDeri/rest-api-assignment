import React,{ Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {

    render() {
      return (
        <div className="cointainer">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <Link className="navbar-brand" to="/">App</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <Link className="nav-item nav-link" to="/NewUser">Add user</Link>
                <Link className="nav-item nav-link" to="/NewGroup">Add group</Link>
              </div>
            </div>
          </nav>
        </div>
      );
    }
  }
  
  export default Header;