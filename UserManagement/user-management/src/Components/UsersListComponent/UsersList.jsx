import React, { Component } from 'react';
import { API_URL } from '../../config';
import { withRouter } from 'react-router-dom'

class UsersList extends Component{

    constructor(){
        super();
        this.state = {
            users: [],
            loading: false,
            error: null,
            usersGroups: [],
        }
        this.handleEdit = this.handleEdit.bind(this);
    }

    componentDidMount(){
        this.fetchUsers();
    }

    fetchUsers(){
        this.setState({loading: true});

        fetch(`${API_URL}Users`)
            .then((response) => {
                return response.json().then(json =>{
                    return response.ok ? json : Promise.reject(json)
                })
            })
            .then((data) => {
                this.setState({
                    users: data,
                    loading: false,
                })
                this.fetchUsersGroups();
            })
            .catch((error) => {
                this.setState({
                    error: error.errorMessage,
                    loading: false
                })
            });
    }

    fetchUsersGroups(){
        let users = this.state.users;
        users.sort(function(a,b){return a.id - b.id});
        users.forEach(user => {
            fetch(`${API_URL}UserGroups/User/${user.id}`)
                .then((response) => {
                    return response.json().then(json => {
                        return response.ok ? json : Promise.reject(json)
                    })
                })
                .then((data) => {
                    let currentUserGroups = this.state.usersGroups;
                    currentUserGroups.push(data.join(', '));

                    this.setState({
                        usersGroups: currentUserGroups,
                        loading: false,
                    })
                })
                .catch((error) => {
                    this.setState({
                        error: error.errorMessage,
                        loading: false,
                    })
                })
        })
    }

    delete(userId){
        fetch(`${API_URL}Users/${userId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({id: userId})
        })
        .then(() => this.fetchUsers())
    }

    handleEdit(userId){
        this.props.history.push(`/EditUser/${userId}`);
    }

    render(){
        const { users } = this.state;
        return(
            <div className="col-lg-12">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">User name</th>
                            <th scope="col">First name</th>
                            <th scope="col">Last name</th>
                            <th scope="col">Groups</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user,index) => (
                            <tr key={user.id}>
                                <td>{index + 1}</td>
                                <td>{user.userName}</td>
                                <td>{user.firstName}</td>
                                <td>{user.lastName}</td>
                                <td>{this.state.usersGroups[index]}</td>
                                <td>
                                    <div className="btn-group">
                                        <button type="button" className="btn btn-primary" onClick={this.delete.bind(this,user.id)}>Delete</button>
                                        <button type="button" className="btn btn-primary" onClick={this.handleEdit.bind(this,user.id)}>Edit</button>
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default withRouter(UsersList)