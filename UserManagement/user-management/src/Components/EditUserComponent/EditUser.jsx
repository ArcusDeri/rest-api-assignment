import React, { Component } from 'react';
import { API_URL } from '../../config'

class EditUser extends Component{

    constructor(props){
        super(props);
        let id = this.props.match.params.id;
        let today = this.getFormattedDate();
        this.state = {
            id: id,
            userName: '',
            firstName: '',
            lastName: '',
            password: '',
            birthDate: today,
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount(){
        this.getUser();
    }
    getFormattedDate(){
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();
        if(dd < 10)
            dd = '0' + dd;
        if(mm < 10)
            mm = '0' + mm;
        return yyyy + '-' + mm + '-' + dd;
    }

    getUser(){
        fetch(`${API_URL}Users/${this.state.id}`)
            .then((response) => {
                return response.json().then(json =>{
                    return response.ok ? json : Promise.reject(json)
                })
            })
            .then((data) => {
                this.setState({
                    userName: data.userName,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    password: data.password,
                    birthDate: data.birthDate.substring(0,10)
                })
            })
            .catch((error) => {
                this.setState({
                    error: error.errorMessage
                })
            });
    }

    handleSubmit(event){
        const { userName, firstName, lastName, password } = this.state;
        if(userName.length <= 3 || firstName.length <= 3 || lastName.length <= 3 || password.length <= 8)
        {
            alert("Some of the data you have typed in is too short.");
            event.preventDefault();
            return;
        }
        fetch(`${API_URL}Users/${this.state.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(this.props.history.push(`/`))
        event.preventDefault();
    }

    handleChange(event){
        const value = event.target.value;
        const propName = event.target.name;
        
        this.setState({ [propName]: value });
    }

    render(){
        const { userName, firstName, lastName, password, birthDate } = this.state;
        return(
            <form 
                className="container"
                onSubmit={this.handleSubmit}
                id="mainForm">
                <h1>Edit user</h1>
                <div className="form-group">
                    <input 
                        name="userName"
                        className="form-control"
                        type="text"
                        placeholder="Username"
                        onChange={this.handleChange}
                        value={userName}
                    />
                </div>
                <div className="form-group">
                    <input 
                        name="firstName"
                        className="form-control"
                        type="text"
                        placeholder="First name"
                        onChange={this.handleChange}
                        value={firstName}
                    />
                </div>
                <div className="form-group">
                    <input 
                        name="lastName"
                        className="form-control"
                        type="text"
                        placeholder="Last name"
                        onChange={this.handleChange}
                        value={lastName}
                    />
                </div>
                <div className="form-group">
                    <input 
                        name="password"
                        className="form-control"
                        type="password"
                        placeholder="Password"
                        onChange={this.handleChange}
                        value={password}
                    />
                </div>
                <div className="form-group">
                    <input 
                        name="birthDate"
                        className="form-control"
                        type="date"
                        placeholder="birthDate"
                        onChange={this.handleChange}
                        value={birthDate}
                    />
                </div>
                <button className="btn btn-primary" type="submit" value="Save">Save</button>
            </form>
        )
    }
}

export default EditUser;