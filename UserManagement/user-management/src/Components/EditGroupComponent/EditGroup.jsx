import React, { Component } from 'react';
import { API_URL } from '../../config'

class EditUser extends Component{

    constructor(props){
        super(props);
        let id = this.props.match.params.id;
        this.state = {
            id: id,
            name: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount(){
        this.getGroup();
    }

    getGroup(){
        fetch(`${API_URL}Groups/${this.state.id}`)
            .then((response) => {
                return response.json().then(json =>{
                    return response.ok ? json : Promise.reject(json)
                })
            })
            .then((data) => {
                this.setState({
                    name: data.name,
                })
            })
            .catch((error) => {
                this.setState({
                    error: error.errorMessage
                })
            });
    }

    handleSubmit(event){
        const { name } = this.state;
        if(name.length <= 3)
        {
            alert("Some of the data you have typed in is too short.");
            event.preventDefault();
            return;
        }
        fetch(`${API_URL}Groups/${this.state.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(this.props.history.push(`/`))
        event.preventDefault();
    }

    handleChange(event){
        const value = event.target.value;
        const propName = event.target.name;
        
        this.setState({ [propName]: value });
    }

    render(){
        const { name } = this.state;
        return(
            <form 
                className="container"
                onSubmit={this.handleSubmit}
                id="mainForm">
                <h1>Edit group</h1>
                <div className="form-group">
                    <input 
                        name="name"
                        className="form-control"
                        type="text"
                        placeholder="Group name"
                        onChange={this.handleChange}
                        value={name}
                    />
                </div>
                <button className="btn btn-primary" type="submit" value="Save">Save</button>
            </form>
        )
    }
}

export default EditUser;