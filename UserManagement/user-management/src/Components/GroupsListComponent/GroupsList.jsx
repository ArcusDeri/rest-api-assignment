import React, { Component } from 'react';
import { API_URL } from '../../config';
import { withRouter } from 'react-router-dom'

class GroupsList extends Component{

    constructor(){
        super();
        this.state = {
            groups: [],
            loading: false,
            error: null,
            groupUsers: [],
        }
    }

    componentDidMount(){
        this.fetchGroups();
    }

    fetchGroups(){
        this.setState({loading: true});

        fetch(`${API_URL}Groups`)
            .then((response) => {
                return response.json().then(json =>{
                    return response.ok ? json : Promise.reject(json)
                })
            })
            .then((data) => {
                this.setState({
                    groups: data,
                    loading: false,
                })
                this.fetchGroupUsers();
            })
            .catch((error) => {
                this.setState({
                    error: error.errorMessage,
                    loading: false
                })
            });
    }

    fetchGroupUsers(){
        this.state.groups.forEach(group => {
            fetch(`${API_URL}UserGroups/Group/${group.id}`)
                .then((response) => {
                    return response.json().then(json => {
                        return response.ok ? json : Promise.reject(json)
                    })
                })
                .then((data) => {
                    let currentGroupsUsers = this.state.groupUsers;
                    currentGroupsUsers.push(data.join(', '));

                    this.setState({
                        groupUsers: currentGroupsUsers,
                        loading: false,
                    })
                })
                .catch((error) => {
                    this.setState({
                        error: error.errorMessage,
                        loading: false,
                    })
                })
        })
    }
    delete(groupId){
        fetch(`${API_URL}Groups/${groupId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({id: groupId})
        })
        .then(() => this.fetchGroups())
    }

    handleEdit(groupId){
        this.props.history.push(`/EditGroup/${groupId}`);
    }

    render(){
        const { groups } = this.state;
        return(
            <div className="col-lg-12">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Group name</th>
                            <th scope="col">Users</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {groups.map((group,index) => (
                            <tr key={group.id}>
                                <td>{index + 1}</td>
                                <td>{group.name}</td>
                                <td>{this.state.groupUsers[index]}</td>
                                <td>
                                    <button className="btn btn-primary" onClick={this.delete.bind(this,group.id)}>Delete</button>
                                    <button type="button" className="btn btn-primary" onClick={this.handleEdit.bind(this,group.id)}>Edit</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default withRouter(GroupsList)