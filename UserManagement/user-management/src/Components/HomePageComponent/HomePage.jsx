import React,{ Component } from 'react';
import UsersList from '../UsersListComponent/UsersList'
import GroupsList from '../GroupsListComponent/GroupsList';

class HomePage extends Component{
    render(){
        return(
            <div className="container h-100">
                <h1>Welcome to the assignment application!</h1>
                <p>
                    This application makes use of React.js and consumes RESTful API made in .NET Core.
                </p>
                <UsersList />
                <GroupsList />
            </div>
        )
    }
}

export default HomePage