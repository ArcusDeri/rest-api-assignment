import React, { Component } from 'react';
import { API_URL } from '../../config'
import { withRouter } from 'react-router-dom';

class AddGroup extends Component{

    constructor(){
        super();
        this.state = {
            name: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    handleSubmit(event){
        const { name } = this.state;
        if(name.length <= 2)
        {
            alert("Some of the data you have typed in is too short.");
            event.preventDefault();
            return;
        }
        fetch(`${API_URL}Groups`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        }).then(this.props.history.push(`/`))
        event.preventDefault();
    }

    handleChange(event){
        const value = event.target.value;
        const propName = event.target.name;
        
        this.setState({ [propName]: value });
    }

    render(){
        const { name } = this.state;
        return (
            <form 
                className="container"
                onSubmit={this.handleSubmit}
                id="mainForm"
            >
                <h1>Create new group</h1>
                <div className="form-group">
                    <input 
                        name="name"
                        className="form-control"
                        type="text"
                        placeholder="Group name"
                        onChange={this.handleChange}
                        value={name}
                    />
                </div>
                <button className="btn btn-primary" type="submit" value="Submit">Submit</button>
            </form>
        )
    }
}

export default withRouter(AddGroup);