import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './Components/HeaderComponent/Header';
import HomePage from './Components/HomePageComponent/HomePage';
import AddUser from './Components/AddUserComponent/AddUser';
import AddGroup from './Components/AddGroupComponent/AddGroup'
import EditUser from './Components/EditUserComponent/EditUser'
import EditGroup from './Components/EditGroupComponent/EditGroup'

class App extends Component {

  render() {
  
    return (
      <BrowserRouter>
        <div className="h-100">
        <Header />
          <Switch>
            <Route path="/" component={HomePage} exact />
            <Route path="/NewUser" component={AddUser} exact />
            <Route path="/NewGroup" component={AddGroup} exact />
            <Route path="/EditUser/:id" component={EditUser} exact />            
            <Route path="/EditGroup/:id" component={EditGroup} exact />            
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;